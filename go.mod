module gitlab.com/diamondburned/6cord

go 1.12

require (
	github.com/alecthomas/chroma v0.6.2
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/atotto/clipboard v0.1.1
	github.com/bwmarrin/dgvoice v0.0.0-20170706020935-3c939eca8b2f
	github.com/bwmarrin/discordgo v0.19.0
	github.com/davecgh/go-spew v1.1.1
	github.com/diamondburned/discordgo v1.1.2
	github.com/diamondburned/go-keyring v0.0.0-20190317041914-0cfca17208f6
	github.com/diamondburned/markdown v0.0.0-20190331092810-a5d3b972382c
	github.com/diamondburned/tcell v1.1.6
	github.com/diamondburned/tview v0.0.0-20190314151909-84ae2ea044c9
	github.com/disintegration/imaging v1.6.0
	github.com/gen2brain/beeep v0.0.0-20190226134152-2cb437b15ebe
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/gomarkdown/markdown v0.0.0-20190222000725-ee6a7931a1e4
	github.com/hashicorp/go-retryablehttp v0.5.2 // indirect
	github.com/marcusolsson/tui-go v0.4.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-sixel v0.0.0-20190216163338-cdfbdd9946b1
	github.com/rivo/tview v0.0.0-20190213202703-b373355e9db4
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/rylio/ytdl v0.5.1 // indirect
	github.com/sahilm/fuzzy v0.1.0
	github.com/saibing/bingo v0.0.0-20190314132233-9b5abfe83c95 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/soniakeys/quant v1.0.0 // indirect
	github.com/sourcegraph/jsonrpc2 v0.0.0-20190106185902-35a74f039c6a // indirect
	github.com/stevenroose/gonfig v0.1.4
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/valyala/fasttemplate v1.0.0
	github.com/zalando/go-keyring v0.0.0-20190208082241-fbe81aec3a07 // indirect
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/net v0.0.0-20190313220215-9f648a60d977 // indirect
	golang.org/x/sys v0.0.0-20190316082340-a2f829d7f35f // indirect
	golang.org/x/tools v0.0.0-20190315214010-f0bfdbff1f9c // indirect
	gopkg.in/hraban/opus.v2 v2.0.0-20180426093920-0f2e0b4fc6cd
	layeh.com/gopus v0.0.0-20161224163843-0ebf989153aa
)
