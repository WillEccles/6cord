package main

import (
	"log"
	"time"

	"github.com/diamondburned/discordgo"
)

const (
	authorFormat  = "\n\n[#%06X::][\"\"]%s[-::] [::d]%s[::-]"
	messageFormat = "\n" + `["%d"]%s ["ENDMESSAGE"]`
)

var (
	highlightInterval = time.Duration(time.Second * 7)
	messageStore      []string
)

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if d == nil || Channel == nil {
		return
	}

	if rstore.Check(m.Author, RelationshipBlocked) && cfg.Prop.HideBlocked {
		return
	}

	// Notify mentions
	go mentionHandler(m)

	if m.ChannelID != Channel.ID {
		c, err := d.State.Channel(m.ChannelID)
		if err == nil {
			c.LastMessageID = m.ID

		} else {
			log.Println(err)
		}

		checkReadState()

		return
	}

	if !isRegularMessage(m.Message) {
		return
	}

	// if m.Author.ID != d.State.User.ID {
	ackMe(m.Message)
	// }

	typing.RemoveUser(&discordgo.TypingStart{
		UserID:    m.Author.ID,
		ChannelID: m.ChannelID,
	})

	// messagerenderer.go
	messageRender <- m.Message
	scrollChat()
}
